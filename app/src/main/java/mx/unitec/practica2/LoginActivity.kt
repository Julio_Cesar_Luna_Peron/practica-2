package mx.unitec.practica2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private lateinit var button: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        button = findViewById(R.id.button)

        button.setOnClickListener {
            if(editTextTextEmailAddress.text.isEmpty()) {
                editTextTextEmailAddress.error = getString(R.string.error_text)
                return@setOnClickListener
            }

            Toast.makeText(this, R. string.welcome_message, Toast .LENGTH_LONG).show()
        }

    }
}